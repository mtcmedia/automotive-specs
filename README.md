# Automotive Specs Base

This package provides the base structure for automotive specs APIs - 
interfaces and enum models for car valuation, specification and risk 
implementations across different APIs.

# Installation
```
composer require mtcmedia/automotive-specs
```

For driver setup you will require setting up the config value of the default 
driver. This is defined in config file that can be published

```
php artisan vendor:publish --provider="Mtc\AutomotiveSpecs\Providers\AutomotiveSpecProvider"
```

# Usage

This package is not aimed at a standalone use but in coupling with 
an actual automotive spec driver like CAP, MotorCheck or others.

System allows registering new Drivers:
```php
use Mtc\AutomotiveSpecs\Facades\AutoSpecs;

AutoSpecs::register('driver-name', function () {
    return new MyDriver();
}); 
```

`AutoSpecs` uses a manager that will select driver defined in 
`automotive_specs.driver` value to run as the main driver.
Driver value can also be set through .env file as  `AUTOMOTIVE_SPECS_DRIVER` key. 
A different active driver can be set using 
`AutoSpecs::setActiveDriver($driver_name)` method.

When driver is set up you can call methods on the driver or manager.

* Check if driver supports functionality
```php
use \Mtc\AutomotiveSpecs\Facades\AutoSpecs;
use \Mtc\AutomotiveSpecs\Contracts\CarValuationService;

$supported = AutoSpecs::supports(CarValuationService::class);
```

* Perform service action

```php
use \Mtc\AutomotiveSpecs\Facades\AutoSpecs;

$supported = AutoSpecs::getCarSpecs($vrm, $mileage);
```

# Contributing 

Please see [Contributing](CONTRIBUTING.md) for details.
