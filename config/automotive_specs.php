<?php

return [
    'driver' => env('AUTOMOTIVE_SPECS_DRIVER', null),

    'manager' => \Mtc\AutomotiveSpecs\AutoSpecManager::class,
];
