<?php

namespace Mtc\AutomotiveSpecs;

class VehicleValuation
{
    public $part_exchange;
    public $retail;

    public function setValuesFromArray(array $values): void
    {
        collect($values)
            ->filter(fn($value, $key) => array_key_exists($key, get_object_vars($this)))
            ->each(fn($value, $key) => $this->{$key} = $value);
    }
}
