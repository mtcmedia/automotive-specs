<?php

namespace Mtc\AutomotiveSpecs\Providers;

use Illuminate\Support\ServiceProvider;
use Mtc\AutomotiveSpecs\AutoSpecManager;

class AutomotiveSpecProvider extends ServiceProvider
{
    public function register()
    {
        parent::register();
        $this->mergeConfigFrom(dirname(__DIR__, 2) . '/config/automotive_specs.php', 'automotive_specs');
    }

    public function boot()
    {
        $this->app->singleton('automotive-specs', function ($app) {
            return new AutoSpecManager($app);
        });

        $this->publishes([
            __DIR__ . '/../../config/automotive_specs.php' => config_path('automotive_specs.php'),
        ], 'config');
    }
}
