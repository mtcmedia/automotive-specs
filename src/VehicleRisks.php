<?php

namespace Mtc\AutomotiveSpecs;

class VehicleRisks
{
    public $finance;
    public $mileage;
    public $at_risk;
    public $write_off;
    public $condition;
    public $scrapped;
    public $stolen;
    public $keepers;
    public $plate_change;
    public $colour_changes;
    public $mot_history;
    public $tax_and_sorn;
    public $origin_and_use;

    public function setValuesFromArray(array $values): void
    {
        collect($values)
            ->filter(fn($value, $key) => array_key_exists($key, get_object_vars($this)))
            ->each(function ($value, $key) {
                $this->{$key} = $value;
            });
    }

    public function toArray(): array
    {
        return collect(get_object_vars($this))
            ->filter()
            ->toArray();
    }
}
