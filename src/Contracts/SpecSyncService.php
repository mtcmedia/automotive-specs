<?php

namespace Mtc\AutomotiveSpecs\Contracts;

use Mtc\AutomotiveSpecs\VehicleSpecification;

interface SpecSyncService
{
    public function getCarSpecs(string $vrm, int $mileage = 0): VehicleSpecification;
}
