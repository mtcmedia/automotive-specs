<?php

namespace Mtc\AutomotiveSpecs\Contracts;

use Mtc\AutomotiveSpecs\VehicleRisks;

interface RiskValuationService
{
    public function getCarRisks(string $vrm): VehicleRisks;
}
