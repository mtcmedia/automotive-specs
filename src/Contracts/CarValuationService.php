<?php

namespace Mtc\AutomotiveSpecs\Contracts;

use Mtc\AutomotiveSpecs\VehicleValuation;

interface CarValuationService
{
    public function getValuation(string $vrm): VehicleValuation;
}
