<?php

namespace Mtc\AutomotiveSpecs;

use Illuminate\Support\Manager;

class AutoSpecManager extends Manager
{
    protected static $driver_config_name = 'automotive_specs.driver';

    public function register($name, \Closure $callback): void
    {
        $this->container['automotive-specs']->extend($name, $callback);
    }

    public function getDefaultDriver(): string
    {
        return $this->container['config'][self::$driver_config_name];
    }

    public function setActiveDriver($name): void
    {
        $this->container['config'][self::$driver_config_name] = $name;
    }

    public function supports(string $interface): bool
    {
        return $this->driver() instanceof $interface;
    }
}
