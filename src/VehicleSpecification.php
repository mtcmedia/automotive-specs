<?php

namespace Mtc\AutomotiveSpecs;

class VehicleSpecification
{
    public $UUID;
    public $make;
    public $model;
    public $model_year;
    public $derivative;
    public $registration_number;
    public $registration_date;
    public $engine_size;
    public $mpg;
    public $co2;
    public $body_style;
    public $colour;
    public $transmission;
    public $fuel_type;
    public $drivetrain;
    public $odometer_value;
    public $odometer_metric;
    public $vin;
    public $doors;
    public $seats;
    public $previous_owner_count;
    public $feature_list = [];

    public function setValuesFromArray(array $values): void
    {
        collect($values)
            ->filter(fn($value, $key) => array_key_exists($key, get_object_vars($this)))
            ->each(fn($value, $key) => $this->{$key} = $value);
    }

    public function setFeatureList(array $feature_list): void
    {
        $this->feature_list = $feature_list;
    }
}
