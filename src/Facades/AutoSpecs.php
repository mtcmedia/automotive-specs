<?php

namespace Mtc\AutomotiveSpecs\Facades;

use Illuminate\Support\Facades\Facade;
use Mtc\AutomotiveSpecs\VehicleRisks;
use Mtc\AutomotiveSpecs\VehicleSpecification;
use Mtc\AutomotiveSpecs\AutoSpecManager;
use Mtc\AutomotiveSpecs\VehicleValuation;

/**
 * @method static register(string $driver_name, \Closure $concrete)
 * @method static VehicleSpecification getCarSpecs(string $vrm, int $mileage = 0)
 * @method static VehicleValuation getValuation(string $vrm, int $mileage = 0)
 * @method static VehicleRisks getCarRisks(string $vrm)
 * @see AutoSpecManager
 *
 */
class AutoSpecs extends Facade
{
    /**
     * Define the facade accessor
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'automotive-specs';
    }
}
