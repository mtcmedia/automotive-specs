# Contributors

## Guidelines:

* PSR-12 standard.
* `$snake_case_variables`
* `camelCaseMethods()`

## Authors
* Martins Fridenbergs
