<?php

namespace Mtc\Tests\Unit;

use Mtc\AutomotiveSpecs\Contracts\CarValuationService;
use Mtc\AutomotiveSpecs\Contracts\RiskValuationService;
use Mtc\AutomotiveSpecs\Contracts\SpecSyncService;
use Mtc\AutomotiveSpecs\Facades\AutoSpecs;
use Mtc\AutomotiveSpecs\VehicleRisks;
use Mtc\AutomotiveSpecs\VehicleSpecification;
use Mtc\AutomotiveSpecs\VehicleValuation;
use Mtc\Tests\DummyService;
use Mtc\Tests\TestCase;

class AutoSpecsTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        AutoSpecs::register('dummy', function () {
            return new DummyService();
        });

        AutoSpecs::setActiveDriver('dummy');
    }

    public function test_setting_dummy_driver()
    {
        self::assertEquals('dummy', AutoSpecs::getDefaultDriver());
    }


    public function test_driver_supports_interface()
    {
        self::assertTrue(AutoSpecs::supports(CarValuationService::class));
        self::assertTrue(AutoSpecs::supports(SpecSyncService::class));
        self::assertTrue(AutoSpecs::supports(RiskValuationService::class));
    }

    public function test_get_valuation()
    {
        self::assertInstanceOf(VehicleValuation::class, AutoSpecs::getValuation('ABC123'));
    }

    public function test_get_risks()
    {
        self::assertInstanceOf(VehicleRisks::class, AutoSpecs::getCarRisks('ABC123'));
    }

    public function test_get_specs()
    {
        self::assertInstanceOf(VehicleSpecification::class, AutoSpecs::getCarSpecs('ABC123'));
    }
}
