<?php

namespace Mtc\Tests;

use Mtc\AutomotiveSpecs\Providers\AutomotiveSpecProvider;

class TestCase extends \Orchestra\Testbench\TestCase
{

    protected function getPackageProviders($app)
    {
        return [
            AutomotiveSpecProvider::class,
        ];
    }
}
