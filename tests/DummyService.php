<?php

namespace Mtc\Tests;

use Mtc\AutomotiveSpecs\Contracts\CarValuationService;
use Mtc\AutomotiveSpecs\Contracts\RiskValuationService;
use Mtc\AutomotiveSpecs\Contracts\SpecSyncService;
use Mtc\AutomotiveSpecs\VehicleRisks;
use Mtc\AutomotiveSpecs\VehicleSpecification;
use Mtc\AutomotiveSpecs\VehicleValuation;

class DummyService implements CarValuationService, RiskValuationService, SpecSyncService
{
    public function getValuation(string $vrm): VehicleValuation
    {
        return new VehicleValuation();
    }

    public function getCarRisks(string $vrm): VehicleRisks
    {
        return new VehicleRisks();
    }

    public function getCarSpecs(string $vrm, int $mileage = 0): VehicleSpecification
    {
        return new VehicleSpecification();
    }
}
