<?php

namespace Mtc\Tests\Unit;

use Mtc\AutomotiveSpecs\VehicleRisks;
use Mtc\Tests\TestCase;

class VehicleRiskTest extends TestCase
{
    public function test_risk_assignment()
    {
        $risks = new VehicleRisks();

        $risks->setValuesFromArray([
            'finance' => false,
            'stolen' => 'Stolen in 2011',
            'mileage' => 1,
            'at_risk' => 2,
            'write_off' => 'Write-off in 2012',
            'condition' => 'Rusted frame',
            'scrapped' => 'Sold for parts',
            'keepers' => 99,
            'plate_change' => 3,
            'colour_changes' => 4,
            'mot_history' => 'No MOT',
            'tax_and_sorn' => 5,
            'origin_and_use' => 6,
        ]);

        self::assertEquals(false, $risks->finance);
        self::assertEquals(1, $risks->mileage);
        self::assertEquals(2, $risks->at_risk);
        self::assertEquals(3, $risks->plate_change);
        self::assertEquals(4, $risks->colour_changes);
        self::assertEquals(5, $risks->tax_and_sorn);
        self::assertEquals(6, $risks->origin_and_use);
        self::assertEquals(99, $risks->keepers);
        self::assertEquals('Stolen in 2011', $risks->stolen);
        self::assertEquals('Write-off in 2012', $risks->write_off);
        self::assertEquals('Rusted frame', $risks->condition);
        self::assertEquals('Sold for parts', $risks->scrapped);
        self::assertEquals('No MOT', $risks->mot_history);
    }

    public function test_toArray()
    {
        $risks = new VehicleRisks();

        $risks->setValuesFromArray([
            'finance' => false,
            'stolen' => 'Stolen in 2011',
        ]);

        self::assertEquals([ 'stolen' => 'Stolen in 2011' ], $risks->toArray());

        $risks2 = new VehicleRisks();
        $risks2->setValuesFromArray([
            'finance' => false,
            'stolen' => 'Stolen in 2011',
            'plate_change' => 3,
        ]);

        self::assertNotEquals([ 'stolen' => 'Stolen in 2011' ], $risks2->toArray());
        self::assertEquals([ 'plate_change' => 3, 'stolen' => 'Stolen in 2011' ], $risks2->toArray());

        $risks3 = new VehicleRisks();
        $risks3->setValuesFromArray([
            'finance' => false,
            'stolen' => 'Stolen in 2011',
            'random' => 3,
        ]);

        self::assertNotEquals([ 'stolen' => 'Stolen in 2011' ], $risks2->toArray());
    }
}
