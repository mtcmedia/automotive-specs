<?php

namespace Mtc\Tests\Unit;

use Mtc\AutomotiveSpecs\VehicleValuation;
use Mtc\Tests\TestCase;

class VehicleValuationTest extends TestCase
{
    public function test_spec_assignment()
    {
        $valuation = new VehicleValuation();

        $valuation->setValuesFromArray([
            'retail' => 12300,
            'part_exchange' => 11230,
        ]);

        self::assertEquals(12300, $valuation->retail);
        self::assertEquals(11230, $valuation->part_exchange);
    }
}
