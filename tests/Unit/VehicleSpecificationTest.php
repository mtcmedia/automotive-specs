<?php

namespace Mtc\Tests\Unit;

use Mtc\AutomotiveSpecs\VehicleSpecification;
use Mtc\Tests\TestCase;

class VehicleSpecificationTest extends TestCase
{
    public function test_spec_assignment()
    {
        $spec = new VehicleSpecification();

        $spec->setValuesFromArray([
            'UUID' => 'abc123',
            'registration_number' => 'AAVC31',
            'make' => 'BMW',
            'model' => '330i',
            'derivative' => 'e92 330i xDrive',
            'model_year' => 2009,
            'registration_date' => '2009-03-04',
            'engine_size' => 2999,
            'mpg' => 18,
            'co2' => 199,
            'body_style' => 'coupe',
            'colour' => 'black',
            'transmission' => 'automatic',
            'fuel_type' => 'petrol',
            'drivetrain' => 'RWD',
            'odometer_value' => 150000,
            'odometer_metric' => 'km',
        ]);

        self::assertEquals('abc123', $spec->UUID);
        self::assertEquals('AAVC31', $spec->registration_number);
        self::assertEquals('BMW', $spec->make);
        self::assertEquals('330i', $spec->model);
        self::assertEquals('e92 330i xDrive', $spec->derivative);
        self::assertEquals(2009, $spec->model_year);
        self::assertEquals('2009-03-04', $spec->registration_date);
        self::assertEquals(2999, $spec->engine_size);
        self::assertEquals(18, $spec->mpg);
        self::assertEquals(199, $spec->co2);
        self::assertEquals('coupe', $spec->body_style);
        self::assertEquals('black', $spec->colour);
        self::assertEquals('automatic', $spec->transmission);
        self::assertEquals('petrol', $spec->fuel_type);
        self::assertEquals('RWD', $spec->drivetrain);
        self::assertEquals(150000, $spec->odometer_value);
        self::assertEquals('km', $spec->odometer_metric);
    }

    public function test_feature_list_assignment()
    {
        $feature_list = [
            'Category' => [
                'name' => 'Name',
                'value' => 'Value'
            ]
        ];
        $spec = new VehicleSpecification();
        $spec->setFeatureList($feature_list);

        self::assertEquals($feature_list, $spec->feature_list);
    }
}
